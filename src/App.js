import { useState } from "react";
import arrs from "./Components/Info"
import Button from "./Components/Button";
import Modal from "./Components/Modal";
import Actions from "./Components/Actions";
import './index.css'


const App = () => {
  const [ modal, setModal ] = useState(null)

  const openModal = (el) => {
    setModal({...el})
  }

  const closeModal = () => {setModal(null)}


  return (
    <>
      <div className="app">
        {arrs.map((el) => {
          return(
            <Button 
              key={el.id}
              backgroundColor={el.btnBackgroung}
              classes={el.btnStyle}
              text={el.btnText}
              onClick={() => {
                openModal(el)
              }}
            />
          )
        })}
  
      </div>

      {
        modal && (
          <Modal 
            header={modal.modalTitle}
            text={modal.modalText}
            closeModal={() => closeModal()}
            closeButton={modal.btnClose}
            actions={<Actions closeModal={() => closeModal()} />}
          />
        )
      }
    </>
  )
}

export default App;
