const arrs = [
    {
      id: 1,
      btnText: "Open first modal",
      btnClose: "true",
      btnStyle: "btn btn-first",
      btnBackgroung: "linear-gradient(to right, rgba(70, 70, 255, 0.6) 30%, rgba(255, 255, 70, 0.8) 100%)",
      modalTitle: "Do you want delete this file?",
      modalText: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
    },
    {
      id: 2,
      btnText: "Open second modal",
      btnClose: "false",
      btnStyle: "btn btn-second",
      btnBackgroung: "linear-gradient(to right, rgba(70, 200, 200, 0.6) 30%, rgba(200, 70, 70, 0.8) 100%)",
      modalTitle: "Do something else then first button",
      modalText: "It mean, that second button show you another modal",
    }
]

export default arrs