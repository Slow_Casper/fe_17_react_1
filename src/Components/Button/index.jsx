import "./button.scss"

const Button = ({ backgroundColor, classes, text, onClick }) => {

    return (
        <button 
            style={{
                backgroundImage: backgroundColor
            }}
            className={classes} 
            type='button'
            onClick={onClick}
        >
            {text}
        </button>
    )
}

export default Button