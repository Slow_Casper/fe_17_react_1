import Button from "../Button";
import "../Button/button.scss"

const Actions = ({ closeModal }) => {

    return(
        <>
            <Button classes="btn btn-modal" text="OK" onClick={null} />
            <Button classes="btn btn-modal" text="CANCEL" onClick={closeModal} />
        </>
    )
}

export default Actions