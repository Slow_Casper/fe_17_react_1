import Button from "../Button"
import styles from "./Modal.module.scss"

const Modal = ({ header, text, closeModal, closeButton, actions }) => {

    return (
        <div className={styles.background} onClick={closeModal}>
            <div className={styles.modal}>
                <div className={styles.top}>
                    <h2  className={styles.title}>{header}</h2>
                    {
                        closeButton === "true" && (<Button classes={styles.close} text="X" onClick={closeModal} />)
                    }
                </div>
                <p className={styles.text}>{text}</p>
                <div className={styles.buttons}>
                    {actions}
                </div>
            </div>
        </div>
    )
} 

export default Modal